
dofile("temperature.lua")
dofile("wifi.lua")
dofile("relay.lua")
dofile("setpoint.lua")

function get_state_json()
  local state = "{"
  .. "\"temperature\":" ..tostring(get_temperature())
  .. ",\"setpoint\":" ..tostring(get_setpoint())
  .. ",\"relay\":" .. tostring(get_relay())
  .. "}"
  return state
end

local function close(sck)
  sck:close()
  collectgarbage()
  print("sck closed "..node.heap())
end

function http_get(sck)
  local res = "HTTP/1.1 200 OK\nContent-Type: application/json\n\n" .. get_state_json() .. "\n"
  sck:send(res)
  sck:on("sent", close)
  print("sck sent "..node.heap())
end

function http_put_setpoint(sck, wtd)
  if set_setpoint(wtd) then
    update()
  end
  http_get(sck)
end

function receiver(sck, request)
  local _, _, method, path, vars = string.find(request, "([A-Z]+) (.+)?(.+) HTTP");
  local query = {}

  print("sck opened "..node.heap())

  if vars == nil then
    _, _, method, path = string.find(request, "([A-Z]+) (.+) HTTP");
    print(" method:" ..method.. " path:" ..path)
  else
    print(" method:" ..method.. " path:" ..path.. " vars:" .. vars)
    for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
      query[k] = v
    end
  end

  if method == "GET" and path == "/" then
    http_get(sck)
  elseif method == "PUT" and path == "/" and query.setpoint then
    http_put_setpoint(sck, query.setpoint)
  else
    sck:close()
  end

end

function update()
  print("...update...")
  local temperature = get_temperature()
  local setpoint = get_setpoint()

  if temperature < setpoint then
    relay_on()
  elseif temperature == setpoint then
  else
    relay_off()
  end

  print(get_state_json())
end

if not tmr.create():alarm(6000, tmr.ALARM_AUTO, update)
then
  print("E: update timer")
end
update()

if srv ~= nil then
  srv:close()
end
srv = net.createServer(net.TCP)
srv:listen(80, function(conn)
  conn:on("receive", receiver)
end)
