
dofile("credentials.lua")

function wifi_connect_event(T)
  print("I: Connected to " .. T.SSID)
  if disconnect_ct ~= nil then
    disconnect_ct = nil
  end
end

function wifi_got_ip_event(T)
  print("I: IP address " .. T.IP)
end

function wifi_disconnect_event(T)
  if T.reason == wifi.eventmon.reason.ASSOC_LEAVE then
    return
  end

  local total_tries = 75
  print("E: Failed to connect to " .. T.SSID)

  for key, val in pairs(wifi.eventmon.reason) do
    if val == T.reason then
      print("I: Disconnect reason "..val.." - "..key)
      break
    end
  end

  if disconnect_ct == nil then
    disconnect_ct = 1
  else
    disconnect_ct = disconnect_ct + 1
  end
  if disconnect_ct < total_tries then
    print("I: Retrying "..(disconnect_ct + 1).." of "..total_tries.."")
  else
    wifi.sta.disconnect()
    print("E: Aborting connection to " .. T.SSID)
    disconnect_ct = nil
  end
end

-- Register WiFi Station event callbacks
wifi.eventmon.register(wifi.eventmon.STA_CONNECTED, wifi_connect_event)
wifi.eventmon.register(wifi.eventmon.STA_GOT_IP, wifi_got_ip_event)
wifi.eventmon.register(wifi.eventmon.STA_DISCONNECTED, wifi_disconnect_event)

print("Connecting to " .. SSID)
wifi.setmode(wifi.STATION)
wifi.sta.config({ssid = SSID, pwd = PASSWORD})
