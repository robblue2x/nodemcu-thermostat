
local state = false
gpio.mode(8, gpio.OUTPUT)
gpio.write(8, gpio.LOW)

function relay_on()
  gpio.write(8, gpio.HIGH)
  state = true
end

function relay_off()
  gpio.write(8, gpio.LOW)
  state = false
end

function get_relay()
  return state
end
