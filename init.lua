
function start_app()
  if not file.exists("application.lua") then
    print("application.lua doesn't exist")
  elseif file.open("init.lua") == nil then
    print("init.lua deleted or renamed")
  else
    print("Running application.lua")
    file.close("init.lua")
    dofile("application.lua")
  end
end

function wait_for_program()
  gpio.mode(1, gpio.INPUT, gpio.PULLUP)
  return gpio.read(1) == 0
end

function boot()
  if not wait_for_program() then
    print("starting app")
    start_app()
  else

    l = file.list();
    for k, v in pairs(l) do
      print("found - name:"..k..", size:"..v)
      if k == "init.lua" then
        print("keeping - name:"..k..", size:"..v)
      else
        print("removing - name:"..k..", size:"..v)
        file.remove(k)
      end
    end
    print("ready to program")

  end
end

tmr.create():alarm(5000, tmr.ALARM_SINGLE, boot)
