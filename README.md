
# flashing tool
https://github.com/espressif/esptool

# ESP-01

## pinouts
http://hackerspace.pbworks.com/w/page/88183850/ESP8266

## GPIO mapping
https://nodemcu.readthedocs.io/en/master/en/modules/gpio/


## forum
http://www.esp8266.com/


## commands

# get chip id
`esptool.py --port /dev/ttyUSB0 chip_id`

# get chip type and flash size
`esptool.py --port /dev/ttyUSB0 flash_id`

# write firmware
`esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ./bins/nodemcu-11-24-integer.bin`

# upload files
`nodemcu-uploader upload *.lua`

# open terminal Ctrl+] to exit
`nodemcu-uploader terminal`
