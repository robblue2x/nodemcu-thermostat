
local setpoint = 22

function get_setpoint()
  return setpoint
end

function set_setpoint(sp)
  local a = tonumber(sp)
  if a ~= nil and a < 40 and a > 10 then
    print("setpoint valid "..a)
    setpoint = a;
    return true
  end
  print("setpoint invalid ")
  return false
end
